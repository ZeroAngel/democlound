package com.anhht.democloud.repositories;

import com.anhht.democloud.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
