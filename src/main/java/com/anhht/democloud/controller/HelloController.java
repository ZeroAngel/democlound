package com.anhht.democloud.controller;

import com.anhht.democloud.model.ResponseObject;
import com.anhht.democloud.repositories.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Log4j2
@RequestMapping(path = "/api/v1/hello")
public class HelloController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/hello_word")
    public ResponseEntity<ResponseObject> hello() {
        log.info("Say Hello");
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("ok", "ALL Done", "SAY HELLO WORD"));
    }

    @GetMapping("fetch-all-user")
    public ResponseEntity<ResponseObject> fetchAllUser() {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "All User Fetched", userRepository.findAll()));
    }

}
